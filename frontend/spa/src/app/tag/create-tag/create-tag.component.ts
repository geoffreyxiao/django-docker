import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TagService } from '../tag.service';

@Component({
  selector: 'app-create-tag',
  templateUrl: './create-tag.component.html',
  styleUrls: ['./create-tag.component.scss'],
})
export class CreateTagComponent implements OnInit {
  @Output() refresh = new EventEmitter();
  public validateForm: FormGroup;
  public tagId: string = '';
  constructor(public tagService: TagService, public fb: FormBuilder) {
    this.validateForm = this.fb.group({
      name: ['', [Validators.required]],
    });
    this.tagService.openModal$.subscribe((params: any) => {
      this.isVisible = true;
      if (params) {
        this.validateForm.patchValue(params);
        this.tagId = params.id;
      }
    });
  }

  get valid() {
    return this.validateForm.valid;
  }

  get data() {
    return this.validateForm.value;
  }

  ngOnInit(): void {}

  isVisible = false;

  close() {
    this.isVisible = false;
    this.validateForm.reset();
  }

  handleOk(): void {
    console.log('Button ok clicked!');
    let params = this.data;
    if (this.tagId) params['id'] = this.tagId;
    const subscription = this.tagId
      ? this.tagService.updateTag(params)
      : this.tagService.createTag(params);
    subscription.subscribe((res) => {
      this.close();
      this.refresh.emit();
    });
  }

  handleCancel(): void {
    console.log('Button cancel clicked!');
    this.close();
  }
}
