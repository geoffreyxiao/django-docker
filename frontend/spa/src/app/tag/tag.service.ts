import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
@Injectable({
  providedIn: 'root',
})
export class TagService {
  public url = '/api/';

  public openModal = new Subject();
  public openModal$ = this.openModal.asObservable();
  constructor(private http: HttpClient) {}

  public getTags(params) {
    return this.http.get(`${this.url}tags/`, { params: params });
  }

  public createTag(params) {
    return this.http.post(`${this.url}tags/`, params);
  }

  public updateTag(params) {
    return this.http.put(`${this.url}tags/${params.id}/`, params);
  }

  public deleteTag(id) {
    return this.http.delete(`${this.url}/tags/${id}/`);
  }

  public open(params?) {
    this.openModal.next(params);
  }
}
