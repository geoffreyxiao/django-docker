import { Component, OnInit } from '@angular/core';
import { TagService } from './tag.service';

interface Tag {
  id: string;
  name: string;
  created: any;
}
@Component({
  selector: 'app-tag',
  templateUrl: './tag.component.html',
  styleUrls: ['./tag.component.scss'],
})
export class TagComponent implements OnInit {
  constructor(private tagService: TagService) {}
  listOfData: Tag[] = [];
  ngOnInit(): void {
    this.getTags();
  }

  open(params?) {
    this.tagService.open(params);
  }

  getTags() {
    let params = {
      ordering: '-created',
    };
    this.tagService.getTags(params).subscribe((res: any) => {
      console.log(res);
      this.listOfData = res['results'];
    });
  }

  delete(id) {
    this.tagService.deleteTag(id).subscribe((res) => {
      this.getTags();
    });
  }
}
