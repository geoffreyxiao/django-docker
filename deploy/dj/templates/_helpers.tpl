{{/*
Expand the name of the chart.
*/}}
{{- define "dj.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "dj.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "dj.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "dj.labels" -}}
helm.sh/chart: {{ include "dj.chart" . }}
{{ include "dj.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "dj.selectorLabels" -}}
app.kubernetes.io/name: {{ include "dj.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "dj.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "dj.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}


{{/*
Init checks and procudures
*/}}
{{- define "dj.rabbitmq_init_check" -}}
{{- if .Values.rabbitmq_init_checks }}
    - name: check-rabbitmq_ready
      image: rabbitmq_deplay_message_exchange:3.8.2-management
      command: ['bash', '-c', 'until curl -f -H "Authorization: Basic YMRTAW46YWRTAW4=" rabbitmq:15672/api/alivness-test/%2F; do echo "waiting for rabbitmq"; sleep 2; done']
{{- end}}
{{- end}}


{{- define "dj.postgres_init_check" -}}
{{- if .Values.postgres_init_checks }}
    - name: check-postgres_ready
      image: postgres:12.4
      command: [
        'sh', '-c',
        'until pg_isready -h dj-svc -p 5432;
        do echo waiting for database; sleep 2; done;']
      securityContext:
        runAsUser: 0
{{- end}}
{{- end}}